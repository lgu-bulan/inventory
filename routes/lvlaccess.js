const express =  require ("express");
const accessOperation = require("../controllers/lvlaccess");
const {
    addLVL1Access, addLVL2Access, addLVL3Access,
    updateLVL1ACCESS, updateLVL2ACCESS, updateLVL3ACCESS,
    getALLLV1LACCESS, getALLLV2LACCESS, getALLLV3LACCESS,
    getLVL1ACCESS, getLVL2ACCESS, getLVL3ACCESS,
    deleteLVL1ACCESS, deleteLVL2ACCESS, deleteLVL3ACCESS
  } = accessOperation

const router = express.Router();


// //[Section] CREATE
    router.post("/", addLVL1Access);
    router.post("/", addLVL2Access);
    router.post("/", addLVL3Access);

// //[Section] UPDATE
    router.delete("/lvlAccess", updateLVL1ACCESS)
    router.delete("/lvlAccess", updateLVL2ACCESS)
    router.delete("/lvlAccess", updateLVL3ACCESS)

//[Section] GET All
   router.get("/lvl1Access", getALLLV1LACCESS);
   router.get("/lvl2Access", getALLLV2LACCESS);
   router.get("/lvl3Access", getALLLV3LACCESS);

 //[Section] GET
    router.get("/lvlAccess", getLVL1ACCESS);
    router.get("/lvlAccess", getLVL2ACCESS);
    router.get("/lvlAccess", getLVL3ACCESS);

//  //[Section] DELETE
    router.delete("/lvlAccess", deleteLVL1ACCESS)
    router.delete("/lvlAccess", deleteLVL2ACCESS)
    router.delete("/lvlAccess", deleteLVL3ACCESS)


module.exports = router