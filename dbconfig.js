const dotenv = require("dotenv");
dotenv.config();

const config = {
    user :'sa',
    password :process.env.DB_password || "",
    server:process.env.DB_server || "",
    database:process.env.DB_database || "",
    options:{
        trustServerCertificate: true,
        trustedconnection: true,
        enableArithAbort : true, 
        instancename :'SQLEXPRESS'
    },
    // environment variables are string for port need int so convert
    port: parseInt(process.env.DB_port, 10) || 5000,
}

module.exports = config;