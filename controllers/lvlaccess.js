const config = require('../dbconfig')
const sql = require('mssql');

  // !CREATE lvl access
  
    //[Section]CREATE LVL1ACCESS   
    module.exports.addLVL1Access = async (req,res) => {

        const { title, iconClassName, hasChild, routeLink} = req.body;
        

    try {
        let pool = await sql.connect(config);

        let insertAccess = await pool.request()
            .input('title', sql.Int, title)
            .input('iconClassName', sql.VarChar, iconClassName)
            .input('hasChild', sql.Bit, hasChild)
            .input('routeLink', sql.VarChar, routeLink)
            
 // execute uses  a stored procedure in the mssqlserver (Table-> Programability -> Stored Procedure)-- to change into query replace execute then put sql query
            .execute('InsertDataLVL1ACCESS');
            
            //  res.json(insertAccess.recordset);
            //  res.json(req.body);
             res.json(true);
            
    }
    catch (err) {
        console.log(err)
        res.status(500).send(err.message)
    }

    }
    //[Section] CREATE LVL2ACCESS
    module.exports.addLVL2Access = async (req,res) => {

        const { title, iconClassName, hasChild, routeLink, level1ParentID} = req.body;
        
    try {
        let pool = await sql.connect(config);

        let insertAccess = await pool.request()
            .input('title', sql.Int, title)
            .input('iconClassName', sql.VarChar, iconClassName)
            .input('hasChild', sql.Bit, hasChild)
            .input('routeLink', sql.VarChar, routeLink)
            .input('level1ParentID', sql.Int, level1ParentID)
            
 // execute uses  a stored procedure in the mssqlserver (Table-> Programability -> Stored Procedure)-- to change into query replace execute then put sql query
            .execute('InsertDataLVL2ACCESS');
            
            //  res.json(insertAccess.recordset);
            res.json(req.body);
            
    }
    catch (err) {
        console.log(err)
        res.status(500).send(err.message)
    }

    }

    //[Section] CREATE LVL3ACCESS
    module.exports.addLVL3Access = async (req,res) => {

        const { title, iconClassName, hasChild, routeLink, level2ParentID} = req.body;

    try {
        let pool = await sql.connect(config);

        let insertAccess = await pool.request()
            .input('title', sql.Int, title)
            .input('iconClassName', sql.VarChar, iconClassName)
            .input('hasChild', sql.Bit, hasChild)
            .input('routeLink', sql.VarChar, routeLink)
            .input('level2ParentID', sql.Int, level2ParentID)
            
 // execute uses  a stored procedure in the mssqlserver (Table-> Programability -> Stored Procedure)-- to change into query replace execute then put sql query
            .execute('InsertDataLVL3ACCESS');
            
            //  res.json(insertAccess.recordset);
            res.json(req.body);
            
    }
    catch (err) {
        console.log(err)
        res.status(500).send(err.message)
    }

    }


 // !GET ALL lvl access
    // level1Access in query is the table name in DB
    // id is the PK in the table (id)

    // [Section] CREATE LVL1 ACCESS
    module.exports.getALLLV1LACCESS = async(req,res,next) =>{

        try{
            let pool = await sql.connect(config);
            let getallAccess = await pool.request().query("SELECT * from level1Access");
            console.log(getallAccess);
            res.json(getallAccess.recordset)

        }
        catch(error){
            console.log(error);
        }
    }
     // [Section] LVL2 ACCESS
     module.exports.getALLLV2LACCESS = async(req,res,next) =>{

        try{
            let pool = await sql.connect(config);
            let getallAccess = await pool.request().query("SELECT * from level2Access");
            console.log(getallAccess);
            res.json(getallAccess.recordset)

        }
        catch(error){
            console.log(error);
        }
    }
      // [Section] LVL3 ACCESS
      module.exports.getALLLV3LACCESS = async(req,res,next) =>{

        try{
            let pool = await sql.connect(config);
            let getallAccess = await pool.request().query("SELECT * from level3Access");
            console.log(getallAccess);
            res.json(getallAccess.recordset)

        }
        catch(error){
            console.log(error);
        }
    }

// !GET BY ID(id) lvl access
    // level1Access in query is the table name in DB
    // id is the PK in the table (id)
    // [Section] GET BY ID(id)LVL 1 ACCESS
    module.exports.getLVL1ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            // let getSingleAccess = await pool.request().query("SELECT * from level1Access ");
            let getSingleAccess = await pool.request()
            .input('id', req.params.id)
            .query("SELECT * from level1Access where id = @id");


            return res.json(getSingleAccess)
    
        }
        catch (error) {
            
            console.log(error);
            return res.json(error)
        }
    }

    // [Section] GET BY ID(id)LVL 2 ACCESS
    module.exports.getLVL2ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let getSingleAccess = await pool.request()
                .input('id', req.params.id)
                .query("SELECT * from level2Access where id = @id");


            return res.json(getSingleAccess.recordset)
    
        }
        catch (error) {
            console.log(error);
        }
    }

    // [Section] GET BY ID(id)LVL 3 ACCESS
    module.exports.getLVL3ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let getSingleAccess = await pool.request()
            .input('id', req.params.id)
                .query("SELECT * from level3Access where id = @id");


            return res.json(getSingleAccess.recordset)
    
        }
        catch (error) {
            console.log(error);
        }
    }


    // !UPDATE
    // [Section] UPDATELVL1 ACCESS
    module.exports.updateLVL1ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let updateSingleAccess = await pool.request()
                .input('id', req.params.id)
                .input('title', sql.Int, title)
                .input('iconClassName', sql.VarChar, iconClassName)
                .input('hasChild', sql.Bit, hasChild)
                .input('routeLink', sql.VarChar, routeLink)

                .query("UPDATE Level1Access SET title = @title ,iconClassName = @iconClassName, hasChild = @hasChild, routeLink = @routeLink WHERE id = @id");


            return res.json(updateSingleAccess.recordset[0])
    
        }
        catch (error) {
            console.log(error);
        }
    }

    // [Section]UPDATE LVL2 ACCESS
    module.exports.updateLVL2ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let updateSingleAccess = await pool.request()
                .input('id', req.params.id)
                .input('title', sql.Int, title)
                .input('iconClassName', sql.VarChar, iconClassName)
                .input('hasChild', sql.Bit, hasChild)
                .input('routeLink', sql.VarChar, routeLink)
                .input('level1ParentID', sql.Int, level2ParentID)

                .query("UPDATE Level2Access SET title = @title ,iconClassName = @iconClassName, hasChild = @hasChild, routeLink = @routeLink, @level1ParentID WHERE id = @id");


            return res.json(updateSingleAccess.recordset[0])
    
        }
        catch (error) {
            console.log(error);
        }
    }

    // [Section]UPDATE LVL3 ACCESS

    module.exports.updateLVL3ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let updateSingleAccess = await pool.request()
                .input('id', req.params.id)
                .input('title', sql.Int, title)
                .input('iconClassName', sql.VarChar, iconClassName)
                .input('hasChild', sql.Bit, hasChild)
                .input('routeLink', sql.VarChar, routeLink)
                .input('level2ParentID', sql.Int, level2ParentID)

                .query("UPDATE Level2Access SET title = @title ,iconClassName = @iconClassName, hasChild = @hasChild, routeLink = @routeLink, @level2ParentID WHERE id = @id");


            return res.json(updateSingleAccess.recordset[0])
    
        }
        catch (error) {
            console.log(error);
        }
    }


    
    // !DELETE
    // [Section] DELETE LVL1 ACCESS
    module.exports.deleteLVL1ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let deletetSingleAccess = await pool.request()
                .input('id', req.params.id)
                .query("DELETE FROM level1Access WHERE id = @id");


            return res.json(deletetSingleAccess.recordset[0])
    
        }
        catch (error) {
            console.log(error);
        }
    }

    // [Section]DELETE LVL2 ACCESS
    module.exports.deleteLVL2ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let deletetSingleAccess = await pool.request()
                .input('id', req.params.id)
                .query("DELETE FROM level2Access WHERE id = @id");


            return res.json(deletetSingleAccess.recordset[0])
    
        }
        catch (error) {
            console.log(error);
        }
    }
    // [Section] DELETE LVL3 ACCESS

    module.exports.deleteLVL3ACCESS = async (req,res) => {
        try {
            let pool = await sql.connect(config);

            let deletetSingleAccess = await pool.request()
                .input('id', req.params.id)
                .query("DELETE FROM level3Access WHERE id = @id");


            return res.json(deletetSingleAccess.recordset[0])
    
        }
        catch (error) {
            console.log(error);
        }
    }
