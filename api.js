   let lvlaccessroutes = require ("./routes/lvlaccess");
    // 
    let express = require("express");
    let bodyParser = require("body-parser");
    let cors = require("cors");
    const dotenv = require("dotenv");
    
    // 

    // 
    dotenv.config();
    let port = process.env.PORT;
    let app = express();

    // 


// [SECTION] Application Routes and Middlewares
    app.use(bodyParser.urlencoded({ extended: true}));
    app.use(bodyParser.json());
    app.use(cors());
    app.use(express.json())

    
    
    // !Routes
    app.use('/api', lvlaccessroutes);

    
    
    
    
    app.listen(port, () => console.log(`API is running at Port: ${port}`));
